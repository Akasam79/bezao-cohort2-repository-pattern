﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Repositories;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Repositories;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using BEZAO_PayDAL.Logics;
using Microsoft.EntityFrameworkCore;

namespace CodeFirstSoln
{
    partial class Program
    {
        static void Main(string[] args)
        {
            //EnrollUser();
            //UpdateUser();

            LoginServices loginServices = new LoginServices();
            string res = loginServices.ShowMainMenu();
            loginServices.AccountsMenu(res);

            UserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            //userService.Delete(8);

            //userService.Get(3);
        }

        static void EnrollUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Register(new RegisterViewModel{FirstName = "Einstein", LastName = "Samuel", Email = "Einstein@simple.com", Username = "Prescotcruz",
                Birthday = new DateTime(2000, 01, 22), Password = "1234@one", ConfirmPassword = "1234@one"});
        }
        static void UpdateUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Update(new UpdateViewModel { Email = "sam4dBabes@gmail.com", NewPassword= "SamDacu",
                CurrentPassword = "yw+QPcc22fEj26F76CBHo36hNftH1ZXPRlDjBptDA0hQ1xun", ConfirmNewPassword = "SamDacu", Username = "Sammy4dBabes"
            });
        }
        
    }
}

