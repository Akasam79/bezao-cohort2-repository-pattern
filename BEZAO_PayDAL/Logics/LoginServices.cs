﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Entities;
using System.Linq;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;

namespace BEZAO_PayDAL.Logics
{
    public class LoginServices
    {
        public int AccountCheck()

        {
            UserService userService = new UserService(new UnitOfWork.UnitOfWork(new BezaoPayContext()));

            int accountNumber = 1;
            int accountFound = 0;
            bool isRunning = true;
            while (isRunning == true)
            {

                LoginViewModel loginViewModel = new LoginViewModel();
                bool isGoodNumber = int.TryParse(Console.ReadLine(), out accountNumber);
                accountFound = userService.Login(new LoginViewModel { accountNumber = accountNumber });
                if (isGoodNumber && accountFound == accountNumber)
                {
                    isRunning = false;
                    return accountFound;
                }
                else if (isGoodNumber && accountNumber == 0)
                {
                    Environment.Exit(1);
                }
                else
                {
                    isGoodNumber = int.TryParse(Console.ReadLine(), out accountNumber);
                    accountFound = userService.Login(new LoginViewModel { accountNumber = accountNumber });
                    if (isGoodNumber && accountFound == accountNumber)
                    {
                        isRunning = false;
                        return accountFound;
                    }
                    else
                    {
                        Environment.Exit(1);
                    }
                }
            }
            return accountFound;
        }

        public string getUserPin()
        {
            int userAccount = AccountCheck();
            using var context = new BezaoPayContext();
            LoginViewModel loginViewModel = new LoginViewModel();
            try
            {
                var userAccountDetail = context.Accounts.Select(row => row);
                var accountId = userAccountDetail.Where(a => a.AccountNumber == userAccount).FirstOrDefault().Id;
                var userDetail = context.Users.Select(row => row);
                var idToFind = userDetail.Where(u => u.Id == accountId).FirstOrDefault().Id;
                var password = userDetail.Where(u => u.Id == accountId).FirstOrDefault().Password;
                var name = userDetail.Where(u => u.Id == accountId).FirstOrDefault().Name;
                Console.WriteLine("Welcome {0}", name);
                Console.WriteLine("Press any key to proceed");
                Console.ReadLine();
                return password;

            }
            catch (Exception e)
            {
                Console.WriteLine("Details for user with account Id {0} not found ", userAccount);
            }
            return "0";
        }

        public void PinCheck()
        {
            try
            {
                bool isRunning = true;
                string truePassword = getUserPin();
                Console.WriteLine("Please enter your password to Proceed or 0 to terminate");
                while (isRunning == true)
                {
                    string password = Console.ReadLine();
                    if (!string.IsNullOrWhiteSpace(password) && string.Equals(truePassword, password))
                    {
                        isRunning = false;
                        Console.WriteLine("Login Successful!");
                    }
                    else if (!truePassword.Equals(password)
                             || string.IsNullOrWhiteSpace(password))
                    {
                        int LoginAttempts = 0;
                        for (int i = 0; i < 2; i++)
                        {
                            Console.WriteLine("Invalid entry, You'll be kicked out after 3 trials");
                            password = Console.ReadLine();
                            if (string.IsNullOrWhiteSpace(password) || !string.Equals(password, truePassword))
                            {
                                Console.WriteLine("Incorrect Entry!");
                                LoginAttempts++;
                            }
                            else if (password == truePassword)
                            {
                                Console.WriteLine("Login Successful!");

                                isRunning = false;
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Login Attempt failed inadvertently");
                                isRunning = false;
                                break;
                            }
                        }
                        if (LoginAttempts > 1)
                            Environment.Exit(1);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Internal server error." + ex);
            }

        }

        public string ShowMainMenu()
        {
            Console.Clear();
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("Welcome to Einstein's LoginValidator.\n");
            Console.WriteLine("please Enter your account Number to proceed");

            bool isRunning = true;
            while (isRunning == true)
            {
                PinCheck();

                Console.WriteLine("\tTransaction Menu\n\t================");
                Console.WriteLine("\t1. Deposit");
                Console.WriteLine("\t2.Transfer ");
                Console.WriteLine("\t3. Withdrawal");
                Console.Write("\n\nPlease enter your choice 1 ... 3 or 0 to exit: ");
                break;
            }
            string menuOptionSelected = Console.ReadLine();
            return menuOptionSelected;
        }

        public void AccountsMenu(string mainMenuOptionSelected)
        {
            TransactionService transactionService = new TransactionService(new UnitOfWork.UnitOfWork(new BezaoPayContext()));
            switch (mainMenuOptionSelected)
            {
                case "0":
                    Environment.Exit(1); //0. Exit Program
                    break;
                case "1":  //1. Withdrawal 
                    DepositCall();
                    break;
                case "2":
                    TransferCall(); //2. Transfer
                    break;
                case "3":
                    WithdrawCall(); //3. Withdrawal
                    break;
                default:
                    Console.Write("Incorrect selection, Please Try Again: 0 ... 3: ");
                    break;
            }

        }
        public static void DepositCall()
        {
            bool isRunning = true;
            while (isRunning == true)
            {
                Console.WriteLine("Please Enter reciepient Account Number");
                int accountNumber;
                bool isGoodNumber = int.TryParse(Console.ReadLine(), out accountNumber);
                if (isGoodNumber)
                {
                    Console.WriteLine("Enter amount to deposit");
                    decimal amount;
                    bool isvalidAmount = decimal.TryParse(Console.ReadLine(), out amount);
                    if (isvalidAmount)
                    {

                        TransactionService transactionService = new TransactionService(new UnitOfWork.UnitOfWork(new BezaoPayContext()));
                        transactionService.Deposit(new DepositViewModel { RecipientAccountNumber = accountNumber, Amount = amount });
                        transactionService.LogDepositTransaction(new DepositViewModel { RecipientAccountNumber = accountNumber, Amount = amount });
                        isRunning = false;
                    }
                    else
                    {
                        Console.WriteLine("Invalid Entry for amount");
                    }

                }
                else
                {
                    Console.WriteLine("invalid entry for account Number");
                }
            }
        }
        public static void TransferCall()
        {
            try
            {
                var context = new BezaoPayContext();
                bool isRunning = true;
                while (isRunning == true)
                {
                    Console.WriteLine("Enter your account Number");
                    bool isValidEntry = int.TryParse(Console.ReadLine(), out int senderAccountNumber);
                    var toGetAccountId = context.Accounts.Where(t => t.AccountNumber == senderAccountNumber).FirstOrDefault();
                    var toGetUser = context.Users.Where(u => u.Id == toGetAccountId.Id).FirstOrDefault();
                    var senderBalance = toGetAccountId.Balance;
                    if (isValidEntry)
                    {
                        Console.WriteLine("Please Enter reciepient Account Number");
                        bool isGoodNumber = int.TryParse(Console.ReadLine(), out int receiverAccountNumber);
                        if (isGoodNumber)
                        {
                            Console.WriteLine("Enter amount to transfer");
                            decimal amount;
                            bool isvalidAmount = decimal.TryParse(Console.ReadLine(), out amount);
                            if (isvalidAmount && senderBalance >= amount)
                            {
                                Console.WriteLine("Enter your password to confirm Transaction");
                                string password = Console.ReadLine();
                                if (!string.IsNullOrWhiteSpace(password) && string.Equals(password, toGetUser.Password))
                                {
                                    TransactionService transactionService = new TransactionService(new UnitOfWork.UnitOfWork(new BezaoPayContext()));
                                    transactionService.Transfer(new TransferViewModel { RecipientAccountNumber = receiverAccountNumber, SenderAccountNumber = senderAccountNumber, Amount = amount });
                                    transactionService.LogTransferTransaction(new TransferViewModel { RecipientAccountNumber = receiverAccountNumber, Amount = amount, SenderAccountNumber = senderAccountNumber });
                                    isRunning = false;
                                }
                                else
                                {
                                    Console.WriteLine("Transaction Failed, Pin is not correct!");
                                }

                            }
                            else
                            {
                                Console.WriteLine("Cannot Process this transaction!. Please that u have enough balance or input is correct");
                            }

                        }
                        else
                        {
                            Console.WriteLine("invalid entry for account Number");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid account Number");
                    }

                }
            }
            catch (Exception)
            {
                Console.WriteLine("Transaction Failed");
            }
            
        }
        public static void WithdrawCall()
        {
            try
            {
                bool isRunning = true;
                while (isRunning == true)
                {
                    var context = new BezaoPayContext();
                    Console.WriteLine("Please Enter your account Number");
                    int accountNumber;
                    bool isGoodNumber = int.TryParse(Console.ReadLine(), out accountNumber);
                    var toGetUserAccount = context.Accounts.Where(p => p.AccountNumber == accountNumber).FirstOrDefault();
                    decimal currentBalance = toGetUserAccount.Balance;
                    if (isGoodNumber)
                    {
                        Console.WriteLine("Enter amount to Withdraw");
                        decimal amount;
                        bool isvalidAmount = decimal.TryParse(Console.ReadLine(), out amount);
                        if (isvalidAmount && currentBalance >= amount)
                        {

                            TransactionService transactionService = new TransactionService(new UnitOfWork.UnitOfWork(new BezaoPayContext()));
                            transactionService.Withdraw(new WithdrawalViewModel { AccountNumber = accountNumber, Amount = amount });
                            transactionService.LogWithdrawalDetail(new WithdrawalViewModel { AccountNumber = accountNumber, Amount = amount });
                            isRunning = false;
                        }
                        else
                        {
                            Console.WriteLine("Cannot Process this transaction!.Please that u have enough balance or input is correct");
                        }

                    }
                    else
                    {
                        Console.WriteLine("invalid entry for account Number");
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Transaction Failed");
            }
            
        }
    }
}
