﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BEZAO_PayDAL.Migrations
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountNumber = table.Column<int>(type: "int", maxLength: 10, nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(38,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Username = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Birthday = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AccountId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ReceiverId = table.Column<int>(type: "int", nullable: false),
                    TransactionMode = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(38,2)", nullable: false),
                    TimeStamp = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "Id", "AccountNumber", "Balance" },
                values: new object[,]
                {
                    { 1, 760015555, 23456782340m },
                    { 2, 222833403, 56000000000m },
                    { 3, 456723646, 78345678230m },
                    { 4, 1642347213, 63723456780m },
                    { 5, 753485382, 88978234000m }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AccountId", "Birthday", "Created", "Email", "IsActive", "Name", "Password", "Username" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(1990, 10, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 8, 26, 20, 21, 32, 55, DateTimeKind.Local).AddTicks(7368), "sorry.sir@abeg.com", true, "Francis Sorry", "fran204", "LowKeyRevFather" },
                    { 2, 2, new DateTime(1420, 10, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 8, 26, 20, 21, 32, 57, DateTimeKind.Local).AddTicks(4338), "badguy@BBA.com", true, "GrandMaster KC", "KCM2884", "Iniit" },
                    { 3, 3, new DateTime(1420, 10, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 8, 26, 20, 21, 32, 57, DateTimeKind.Local).AddTicks(4431), "dara.sage@ned.com", true, "Dara John", "DaraJ213", "DaraIshiazu" },
                    { 4, 4, new DateTime(1420, 10, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 8, 26, 20, 21, 32, 57, DateTimeKind.Local).AddTicks(4437), "sadboy@BBA.com", true, "Kachi !Thename", "Kachi.js.cs", "KachiArgument" },
                    { 5, 5, new DateTime(1420, 10, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 8, 26, 20, 21, 32, 57, DateTimeKind.Local).AddTicks(4440), "omo@BBA.com", true, "Sammy ROCBAFDEZ", "ROCBAFDEZ", "Sammy4dBabes" }
                });

            migrationBuilder.InsertData(
                table: "Transactions",
                columns: new[] { "Id", "Amount", "ReceiverId", "TimeStamp", "TransactionMode", "UserId" },
                values: new object[,]
                {
                    { 1, 50000000m, 0, new DateTime(2021, 10, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1 },
                    { 2, 60000000m, 0, new DateTime(2022, 10, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2 },
                    { 3, 70000000m, 0, new DateTime(2023, 10, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 3 },
                    { 4, 80000000m, 0, new DateTime(2024, 10, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 4 },
                    { 5, 90000000m, 0, new DateTime(2025, 10, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserId",
                table: "Transactions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_AccountId",
                table: "Users",
                column: "AccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Accounts");
        }
    }
}
