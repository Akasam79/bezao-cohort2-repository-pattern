﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Cryptography;

namespace BEZAO_PayDAL.Services
{
  public  class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddMany(IList<User> UserToAdd)
        {
            _unitOfWork.Users.AddRange(UserToAdd);
            _unitOfWork.Commit();
        }
        public void Register(RegisterViewModel model)
        {

            if (!Validate(model))
            {
                return;
            }

            var user = new User
            {
                Name = $"{model.FirstName} {model.LastName}",
                Email = model.Email,
                Username = model.Username,
                Birthday = model.Birthday,
                IsActive = true,
                Password = model.ConfirmPassword,
                Account = new Account{AccountNumber = 1209374652},
                Created = DateTime.Now
            };
            _unitOfWork.Users.Add(user);
            _unitOfWork.Commit();
            Console.WriteLine("Success!");
        }

        public void Update(UpdateViewModel model)
        {
            var user = new User()
            {
                Email = $"{model.Email}",
                Password = model.NewPassword
            };
            try
            {
                string newPassword = user.Password;
                
                using var context = new BezaoPayContext();
                string confirmPassword = model.ConfirmNewPassword;

                var IdToFind = context.Users.Where(c => string.Equals(c.Username, model.Username)).FirstOrDefault().Id;
                User userToUpdate = context.Users.Find(IdToFind);
                string oldPassword = userToUpdate.Password;
                if (string.Equals(newPassword, confirmPassword) && string.Equals(oldPassword, model.CurrentPassword))
                {
                    if (userToUpdate != null && string.Equals(userToUpdate.Username, model.Username, StringComparison.OrdinalIgnoreCase))
                    {
                        userToUpdate.Email = user.Email;
                        userToUpdate.Password = user.Password;

                        if (context.Entry(userToUpdate).State != EntityState.Modified)
                        {
                            throw new Exception("Unable to Modify record o, no vex");
                        }
                        _unitOfWork.Users.Update(userToUpdate);
                        _unitOfWork.Commit();
                        Console.WriteLine("Record has been updated");
                    }

                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("Invalid UserName");
                    }
                }
                else
                {
                    Console.WriteLine("check failed");
                }

            }
            catch (Exception)
            {
                Console.WriteLine($"\t \b user with userName {model.Username} not found");
            }
            
        }
        public string HashNewPassword(UpdateViewModel model)
        {
            string newPassword = model.NewPassword;
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            var pbkdf2 = new Rfc2898DeriveBytes(newPassword, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            string savedPasswordHash = Convert.ToBase64String(hashBytes);
            return savedPasswordHash;
        }
        public string HashConfirmPassword(UpdateViewModel model)
        {
            string confirmPassword = model.ConfirmNewPassword;
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            var pbkdf2 = new Rfc2898DeriveBytes(confirmPassword, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            string savedPasswordHash = Convert.ToBase64String(hashBytes);
            return savedPasswordHash;
        }

        public int Login(LoginViewModel model)
        {
            using (var context = new BezaoPayContext())
            {
                try
                {
                    var accountToFind = context.Accounts.Where(c => c.AccountNumber == model.accountNumber).FirstOrDefault().AccountNumber;
                    return accountToFind;
                }
                catch (Exception)
                {
                    Console.WriteLine("Account does not exist.");
                }

            }
            return 0;
        }

        public void Delete(int id)
        {
            using (var context = new BezaoPayContext())
            {
                User userToDelete = context.Users.Find(id);
                if (userToDelete != null)
                {
                    _unitOfWork.Users.Delete(userToDelete);
                    Account accountToDelete = context.Accounts.Find(id);
                    if (accountToDelete != null)
                    {
                        _unitOfWork.Accounts.Delete(accountToDelete);
                    }
                    _unitOfWork.Commit();
                    Console.WriteLine("Record has been deleted o, Ok, see what we have now if you don't believe me.....");
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine($"\t \b user with id {id} not found");
                }
            }
        }

        public void Get(int id)
        {
            
            using (var context = new BezaoPayContext())
            {
                try
                {
                    var userToDisplay = context.Users.Where(u => u.Id == id).FirstOrDefault();
                    Console.WriteLine($"Id: {userToDisplay.Id}\nName: {userToDisplay.Name}\nEmail: {userToDisplay.Email} \nUser Name: {userToDisplay.Username}");
                }
                catch(Exception)
                {
                    Console.WriteLine("User Details Not Found");
                }
                
                
            }
        }

        private bool Validate(RegisterViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Email) 
                || string.IsNullOrWhiteSpace(model.FirstName) 
                || string.IsNullOrWhiteSpace(model.LastName) 
                || (model.Birthday == new DateTime()) 
                || (string.IsNullOrWhiteSpace(model.Password))
                || (model.Password != model.ConfirmPassword))
            {
                Console.WriteLine("A field is required");
                return false;

            }

            return true;

        }
      
        

        
        

        
    }
}
