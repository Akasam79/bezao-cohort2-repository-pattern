﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Entities;
using System.Linq;
using BEZAO_PayDAL.UnitOfWork;
using BEZAO_PayDAL.Logics;

namespace BEZAO_PayDAL.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TransactionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Deposit(DepositViewModel model)
        {
            var account = new Account()
            {
                Balance = model.Amount,
            };
            try
            {
                int accountToDepositTo = model.RecipientAccountNumber;
                using var context = new BezaoPayContext();
                var accountToFind = context.Accounts.Where(a => a.AccountNumber == accountToDepositTo ).FirstOrDefault();
                var receiverName = context.Users.Where(u => u.AccountId == accountToFind.Id).FirstOrDefault().Name;
                Console.WriteLine("Do you want to top up {0} account with #{1}?", receiverName, model.Amount );
                Console.WriteLine("\tConfirm Deposit\n\t================");
                Console.WriteLine("\t1. Yes");
                Console.WriteLine("\t2. No");
                Console.Write("\n\nPlease enter your choice 1 ... 2 or 0 to exit: ");
                bool isRunning = true;
                while(isRunning == true)
                {
                    int input;
                    bool isGoodNumber = int.TryParse(Console.ReadLine(), out input);
                    switch (input)
                    {
                        case 0:
                            Environment.Exit(1);
                            break;
                        case 1:
                            decimal newBalance = account.Balance + accountToFind.Balance;
                            Console.WriteLine("Successful");
                            isRunning = false;
                            break;
                        case 2:
                            LoginServices loginServices = new LoginServices();
                            loginServices.ShowMainMenu();
                            break;

                    }
                }
                
                
            }
            catch (Exception)
            {
                Console.WriteLine($"\t \b user with account number {model.RecipientAccountNumber} not found");
            }

        }
        public void LogDepositTransaction(DepositViewModel model)
        {
            try
            {
                var context = new BezaoPayContext();
                var transactionDetail = context.Accounts.Where(u => u.AccountNumber == model.RecipientAccountNumber).FirstOrDefault();

                var transaction = new Transaction
                {
                    UserId = transactionDetail.Id,
                    ReceiverId = transactionDetail.Id,
                    TransactionMode = TransactionMode.Credit,
                    Amount = model.Amount,
                    TimeStamp = DateTime.Now
                };
                var newBalance = transactionDetail.Balance += model.Amount;
                var account = new Account
                {
                    Balance = newBalance
                };
                var accountToUpdate = context.Accounts.Find(transactionDetail.Id);
                accountToUpdate.Balance = account.Balance;
                _unitOfWork.Accounts.Update(accountToUpdate);
                _unitOfWork.Commit();
                Console.WriteLine("Your account has been topped up successfully!");

                if (!Validate(transaction))
                {
                    return;
                }

                _unitOfWork.Transactions.Add(transaction);
                _unitOfWork.Commit();
                Console.WriteLine("Deposit Successful!");
            }
            catch(Exception)
            {
                Console.WriteLine("Transaction Failed!");
            }
            
        }
        public void LogTransferTransaction(TransferViewModel model)
        {
            try
            {
                var context = new BezaoPayContext();
                var receiverTransactionDetail = context.Accounts.Where(u => u.AccountNumber == model.RecipientAccountNumber).FirstOrDefault();

                var senderTransactionDetail = context.Accounts.Where(s => s.AccountNumber == model.SenderAccountNumber).FirstOrDefault();

                var transaction = new Transaction
                {
                    UserId = senderTransactionDetail.Id,
                    ReceiverId = receiverTransactionDetail.Id,
                    TransactionMode = TransactionMode.Debit,
                    Amount = model.Amount,
                    TimeStamp = DateTime.Now
                };
                var receiverBalance = receiverTransactionDetail.Balance += model.Amount;
                var senderBalance = senderTransactionDetail.Balance -= model.Amount;

                var accountCredited = new Account
                {
                    Balance = receiverBalance,
                };
                var accountDebited = new Account
                {
                    Balance = senderBalance
                };
                var accountToCredit = context.Accounts.Find(receiverTransactionDetail.Id);
                accountToCredit.Balance = accountCredited.Balance;
                _unitOfWork.Accounts.Update(accountToCredit);

                var accountToDebit = context.Accounts.Find(senderTransactionDetail.Id);
                accountToDebit.Balance = accountDebited.Balance;
                _unitOfWork.Accounts.Update(accountToDebit);
                if (!Validate(transaction))
                {
                    return;
                }
                _unitOfWork.Transactions.Add(transaction);
                _unitOfWork.Commit();
                Console.WriteLine("Transfer Successful!");
            }
            catch (Exception)
            {
                Console.WriteLine("Transaction Failed!");
            }
            
        }
        private bool Validate(Transaction model)
        {
            if (string.IsNullOrWhiteSpace(model.UserId.ToString())
                || string.IsNullOrWhiteSpace(model.ReceiverId.ToString())
                || string.IsNullOrWhiteSpace(model.Amount.ToString()))
            {
                Console.WriteLine("Incomplete parameters were passed");
                return false;

            }

            return true;

        }


        public void Transfer(TransferViewModel model)
        {
            var account = new Account()
            {
                Balance = model.Amount,
            };
            try
            {
                int accountToDepositTo = model.RecipientAccountNumber;
                using var context = new BezaoPayContext();
                var accountToFind = context.Accounts.Where(a => a.AccountNumber == accountToDepositTo).FirstOrDefault();
                var receiverName = context.Users.Where(u => u.AccountId == accountToFind.Id).FirstOrDefault().Name;
                Console.WriteLine("Do you want to transfer #{0} to {1}?",  model.Amount, receiverName);
                Console.WriteLine("\tConfirm Deposit\n\t================");
                Console.WriteLine("\t1. Yes");
                Console.WriteLine("\t2. No");
                Console.Write("\n\nPlease enter your choice 1 ... 2 or 0 to exit: ");
                bool isRunning = true;
                while (isRunning == true)
                {
                    int input;
                    bool isGoodNumber = int.TryParse(Console.ReadLine(), out input);
                    switch (input)
                    {
                        case 0:
                            Environment.Exit(1);
                            break;
                        case 1:
                            decimal newBalance = account.Balance + accountToFind.Balance;
                            Console.WriteLine("Successful");
                            isRunning = false;
                            break;
                        case 2:
                            LoginServices loginServices = new LoginServices();
                            loginServices.ShowMainMenu();
                            break;

                    }
                }


            }
            catch (Exception)
            {
                Console.WriteLine($"\t \b user with account number {model.RecipientAccountNumber} not found");
            }

        }

        public void Withdraw(WithdrawalViewModel model)
        {
            var account = new Account()
            {
                Balance = model.Amount,
            };
            try
            {
                int accountNumber = model.AccountNumber;
                using var context = new BezaoPayContext();
                var accountToFind = context.Accounts.Where(a => a.AccountNumber == accountNumber).FirstOrDefault();
                var receiverName = context.Users.Where(u => u.AccountId == accountToFind.Id).FirstOrDefault().Name;
                Console.WriteLine("Hello {0}, do you want to withdraw {1} from your account?", receiverName, model.Amount);
                Console.WriteLine("\tConfirm Deposit\n\t================");
                Console.WriteLine("\t1. Yes");
                Console.WriteLine("\t2. No");
                Console.Write("\n\nPlease enter your choice 1 ... 2 or 0 to exit: ");
                bool isRunning = true;
                while (isRunning == true)
                {
                    int input;
                    bool isGoodNumber = int.TryParse(Console.ReadLine(), out input);
                    switch (input)
                    {
                        case 0:
                            Environment.Exit(1);
                            break;
                        case 1:
                            decimal newBalance = account.Balance - accountToFind.Balance;
                            Console.WriteLine("Successful");
                            isRunning = false;
                            break;
                        case 2:
                            LoginServices loginServices = new LoginServices();
                            loginServices.ShowMainMenu();
                            break;

                    }
                }


            }
            catch (Exception)
            {
                Console.WriteLine($"\t \b user with account number {model.AccountNumber} not found");
            }
        }

        public void LogWithdrawalDetail(WithdrawalViewModel model)
        {
            try
            {
                var context = new BezaoPayContext();
                var transactionDetail = context.Accounts.Where(u => u.AccountNumber == model.AccountNumber).FirstOrDefault();

                var transaction = new Transaction
                {
                    UserId = transactionDetail.Id,
                    ReceiverId = transactionDetail.Id,
                    TransactionMode = TransactionMode.Debit,
                    Amount = model.Amount,
                    TimeStamp = DateTime.Now
                };
                var newBalance = transactionDetail.Balance -= model.Amount;
                var account = new Account
                {
                    Balance = newBalance
                };
                var accountToUpdate = context.Accounts.Find(transactionDetail.Id);
                accountToUpdate.Balance = account.Balance;
                _unitOfWork.Accounts.Update(accountToUpdate);
                if (!Validate(transaction))
                {
                    return;
                }

                _unitOfWork.Transactions.Add(transaction);
                _unitOfWork.Commit();
                Console.WriteLine("Withdrawal Successful!");
            }
            catch (Exception)
            {
                Console.WriteLine("Transaction Failed!");
            }
            
        }
    }
}
